package week7day1.pages;


import org.openqa.selenium.support.FindBy.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;



import wdMethods.ProjectMethods;

public class FindLead extends ProjectMethods {
	
	public FindLead() {
		PageFactory.initElements(driver,this);
	}

	@FindBy(how = How.XPATH,using = "(//div[@class='x-form-item x-tab-item'])[2]//input)")

	WebElement fname;
	public FindLead enterName(String data)
	{
		type(fname,data);
		return this;
		
	}
	@FindBy(how = How.XPATH,using = "//button[text()='Find Leads']")

	WebElement fleadb;
	public FindLead ClickfindleadName() throws InterruptedException
	{
		click(fleadb);
		Thread.sleep(5000);	
		return this;
		
	}
		
}
