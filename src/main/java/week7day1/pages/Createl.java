package week7day1.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Createl extends ProjectMethods {
	
	 Createl(){
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(how = How.ID,using = "createLeadForm_companyName")
	WebElement cname;
	public Createl typeCName(String data)

	{
		type(cname,data);
	return this;

	}
	
	

	@FindBy(how = How.ID,using = "createLeadForm_firstName")
	WebElement fname;
	public Createl typeFName(String data)

	{
		type(fname,data);
	return this;

	}
	
	@FindBy(how = How.ID,using = "createLeadForm_lastName")
	WebElement lname;
	public Createl typeLName(String data)

	{
		type(lname,data);
	return this;

	}
	
	
	@FindBy(how = How.XPATH,using = "//input[@class='smallSubmit']")
	WebElement clead;
	public ViewLead ClickCL()

	{
		click(clead);
	return new ViewLead();

	}
	
			
	
}
