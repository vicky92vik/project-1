package week7day1.pages;


import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBy.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;



import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {
	
	 public HomePage(){
		PageFactory.initElements(driver,this);
		
	}
@FindBy(how = How.LINK_TEXT,using = "CRM/SFA")
WebElement lname;
public MyHome clickCRF()

{
	click(lname);
return new MyHome();

}

}