package week7day1.pages;


import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBy.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;



import wdMethods.ProjectMethods;

public class MyHome extends ProjectMethods {
	
	MyHome(){
		PageFactory.initElements(driver,this);
		
	}
@FindBy(how = How.LINK_TEXT,using = "Create Lead")
WebElement lb;
public MyLeads clickLead()

{
	click(lb);
return new MyLeads();

}

}