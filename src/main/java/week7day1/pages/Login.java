package week7day1.pages;


import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBy.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;



import wdMethods.ProjectMethods;

public class Login extends ProjectMethods {
	
	public Login(){
		PageFactory.initElements(driver,this);
		
	}
@FindBy(how = How.ID, using = "username" )
WebElement uname;
public Login typeUserName(String data)

{
	type(uname,data);
return this;	
}

@FindBy(how = How.ID,using = "password")
WebElement pname;
public Login typePName(String data)

{
	type(pname,data);
 //throw new RuntimeException();	 
return this;


}
@FindBy(how = How.CLASS_NAME,using = "decorativeSubmit")
WebElement cname;
public HomePage clickLogin()

{
	click(cname);
return new HomePage();

}

}